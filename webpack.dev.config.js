const HtmlWebpackPlugin    = require('html-webpack-plugin');
const merge                = require('webpack-merge');
const baseConfig           = require('./webpack.base.config.js');
const path                 = require('path');

const devConfiguration = env => {
  return merge([
    {
      mode: 'development',
      context: __dirname,
      devServer: {
        historyApiFallback: true,
        contentBase: './dist',
        compress: true,
        headers: {
          'Access-Control-Allow-Origin': '*'
        },
        hot: false,
        inline: false,
        port: 8085,
        host: '0.0.0.0'
      },
      devtool: "inline-sourcemap",
      output: {
        publicPath: "/"
      },
      plugins: [
        new HtmlWebpackPlugin({
          template: path.join(__dirname, './dist/index.html'),
          filename: "index.html"
        })
      ]
    }
  ]);
};

module.exports = env => {
  return merge(baseConfig(env), devConfiguration(env));
};