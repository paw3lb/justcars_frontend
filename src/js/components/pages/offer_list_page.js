import React from "react";
import { connect } from "react-redux";
import { Col, Row } from 'react-bootstrap';
import { fetchOffers } from '../../actions/offer_actions';
import OfferGallery from "../offers/offer_gallery";
import OfferTiles from "../offers/offer_tiles";
import Filters from "../offers/filters";
import QueryString from 'query-string';

export class OfferListPage extends React.Component {

  constructor(props) {
    super(props);
    const values = QueryString.parse(this.props.location.search)
    this.state = {
      params: {
        priceFrom: values.priceFrom || '',
        priceTo: values.priceTo || '',
        title: values.title || '' }
    };

    this.offerClickCallback= this.offerClickCallback.bind(this);
    this.filterOffersCallback= this.filterOffersCallback.bind(this);
  }

  offerClickCallback(e, offerId) {
    e.preventDefault();
    this.props.history.push(`/offers/${offerId}`);
  }

  filterOffersCallback(params) {
    this.setState({ params: params });
    if (!Object.values(params).every(function(i) { return i === ''; })) {
      this.props.history.push(`/?${QueryString.stringify(params)}`);
    } else {
      this.props.history.push('/');
    }
    this.props.fetchOffers(this.prepareSearchParams(params));
  }

  prepareSearchParams(params) {
    const searchParams = {
      'q[price_gteq]': params.priceFrom || null,
      'q[price_lteq]': params.priceTo || null,
      'q[title_cont]': params.title || null
    };
    return searchParams;
  }

  componentDidMount() {
    this.props.fetchOffers(this.prepareSearchParams(this.state.params));
  }

  render() {
    const { offers } = this.props;

    return(
      <div className="offers">
        <Row>
          <Col xs={12} md={12} lg={12}>
            <Filters filterOffersCallback={this.filterOffersCallback} searchParams={this.state.params}/>
          </Col>
        </Row>
        <Row>
          <Col xs={6} md={8} lg={8}>
            <OfferGallery offers={offers}/>
          </Col>
          <Col xs={6} md={4} lg={4}>
            <OfferTiles offers={offers} offerClickCallback={this.offerClickCallback}/>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    offers: state.offers.all
  }
};

const mapDispatchToProps = (dispatch) => ({
  fetchOffers: (params) => dispatch(fetchOffers(params))
});

const WrappedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferListPage);

export default WrappedComponent;