import React from "react";
import { connect } from "react-redux";
import { Col, Row } from 'react-bootstrap';
import { fetchOffer } from '../../actions/offer_actions';
import OfferDetails from "../offers/offer_details";
import { Link } from 'react-router-dom';

export class OfferViewPage extends React.Component {

  componentDidMount() {
    const offerId = this.props.match.params.id;
    this.props.fetchOffer(offerId);
  }

  render() {
    const { offer } = this.props;

    if (!offer) return null;

    return(
      <Row>
        <Col xs={12} md={12} lg={12}>
          <div className="navigation text-center">
            <Link to='/' className="btn btn-default">Powrót do listy ogłoszeń</Link>
          </div>
          <OfferDetails offer={offer}/>
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    offer: state.offers.offer
  }
};

const mapDispatchToProps = (dispatch) => ({
  fetchOffer: (id) => dispatch(fetchOffer(id))
});

const WrappedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferViewPage);

export default WrappedComponent;