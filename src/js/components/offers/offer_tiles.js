import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import OfferTile from "../offers/offer_tile";

export class OfferTiles extends React.Component {

  render() {
    const { offers, offerClickCallback } = this.props;

    if(!offers) return null;

    const tiles = offers.valueSeq().map((offer) => <OfferTile key={offer.get("id")} offer={offer} offerClickCallback={offerClickCallback}/>);

    return (
      <Row>
        <Col xs={12} md={12} lg={12}>
          {tiles}
        </Col>
      </Row>
    )
  }

}

OfferTiles.propTypes = {
  offers: PropTypes.object,
  offerClickCallback: PropTypes.func.isRequired
};

export default OfferTiles;