import React from 'react';
import { Col, Row } from 'react-bootstrap';
import PropTypes from "prop-types";
import SimpleReactValidator from 'simple-react-validator';

export class Filters extends React.Component {

  constructor(props) {
    super(props);
    this.state = props.searchParams;
    this.validator = new SimpleReactValidator({ locale: 'en' });
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleResetFilters = this.handleResetFilters.bind(this);
  }

  handleOnSubmit(e) {
    e.preventDefault();
    if (this.validator.allValid()) {
      const { priceFrom, priceTo, title } = this.state;
      let params = {};
      if (priceFrom) params.priceFrom = priceFrom;
      if (priceTo) params.priceTo = priceTo;
      if (title) params.title = title;
      this.props.filterOffersCallback(params);
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  handleResetFilters(e) {
    e.preventDefault();
    const defaultParams = { priceFrom: '', priceTo: '', title: '' };
    this.setState(defaultParams)
    this.props.filterOffersCallback(defaultParams);
  }

  handleOnChange(e, type) {
    e.preventDefault();
    let params = {};
    params[type] = e.target.value;
    this.setState(params);
  }

  render() {
    return (
      <Row>
        <Col xs={12} md={12} lg={12}>
          <div className="offer-tile well">
            <Row>
              <Col xs={12} md={12} lg={12}>
                <h5>Filtry wyszukiwania:</h5>
                <form className="form-inline">
                  <div className="form-group">
                    <label htmlFor="offer-price-from">Cena od:</label>
                    <input type="text" className="form-control" id="offer-price-from" value={this.state.priceFrom}
                           onChange={(e) => this.handleOnChange(e, 'priceFrom')}/>
                    {this.validator.message('price_from', this.state.priceFrom, 'numeric')}
                  </div>
                  <div className="form-group">
                    <label htmlFor="offer-price-to">Cena do:</label>
                    <input type="text" className="form-control" id="offer-price-to" value={this.state.priceTo}
                           onChange={(e) => this.handleOnChange(e, 'priceTo')}/>
                    {this.validator.message('price_to', this.state.priceTo, 'numeric')}
                  </div>
                  <div className="form-group">
                    <label htmlFor="title">Tytył:</label>
                    <input type="text" className="form-control" id="title"  value={this.state.title}
                           onChange={(e) => this.handleOnChange(e, 'title')}/>
                    {this.validator.message('title', this.state.title, 'string')}
                  </div>
                  <button type="submit" className="btn btn-primary" onClick={(e) => this.handleOnSubmit(e)}>Filtruj</button>
                  <a href="#" className="btn btn-default" onClick={(e) => this.handleResetFilters(e)}>Resetuj</a>
                </form>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    )
  }

}

Filters.propTypes = {
  filterOffersCallback: PropTypes.func.isRequired,
  searchParams:  PropTypes.object.isRequired
};

export default Filters;