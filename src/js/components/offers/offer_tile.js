import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import CurrencyFormat from 'react-currency-format';

export class OfferTile extends React.Component {

  constructor(props) {
    super(props);

    this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick(e, offerId) {
    this.props.offerClickCallback(e, offerId);
  }

  render() {
    const { offer } = this.props;

    return (
      <Row>
        <Col xs={12} md={12} lg={12}>
          <div className="offer-tile well" onClick={(e) => this.handleOnClick(e, offer.get('id'))}>
            <Row>
              <Col xs={12} md={12} lg={12}>
                <h4>{offer.get("title")}</h4>
              </Col>
            </Row>
            <Row>
              <Col xs={4} md={4} lg={4}>
                <img className="tile-img" src={offer.get("imageUrl")}/>
              </Col>
              <Col xs={8} md={8} lg={8}>
                <span className='tile-price'>
                  Cena: <CurrencyFormat decimalScale={1} thousandSeparator=' ' value={offer.get("price")} suffix="zł" displayType={'text'}/>
                </span>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    )
  }

}

OfferTile.propTypes = {
  offer: PropTypes.object.isRequired,
  offerClickCallback: PropTypes.func.isRequired
};

export default OfferTile;