import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import CurrencyFormat from "react-currency-format";

export class OfferDetails extends React.Component {

  render() {
    const { offer } = this.props;

    return (
      <Row>
        <Col xs={12} md={12} lg={12}>
          <form className="form-horizontal">
            <div className="form-group">
              <label className="col-sm-2 control-label">Tytuł:</label>
              <div className="col-sm-10">
                <p className="form-control-static">{offer.get("title")}</p>
              </div>
            </div>
            <div className="form-group">
              <label className="col-sm-2 control-label">Marka:</label>
              <div className="col-sm-10">
                <p className="form-control-static">{offer.get("make_name")}</p>
              </div>
            </div>
            <div className="form-group">
              <label className="col-sm-2 control-label">Model:</label>
              <div className="col-sm-10">
                <p className="form-control-static">{offer.get("model_name")}</p>
              </div>
            </div>
            <div className="form-group">
              <label className="col-sm-2 control-label">Opis:</label>
              <div className="col-sm-10">
                <p className="form-control-static">{offer.get("description")}</p>
              </div>
            </div>
            <div className="form-group">
              <label className="col-sm-2 control-label">Cena:</label>
              <div className="col-sm-10">
                <p className="form-control-static">
                  <CurrencyFormat decimalScale={1} thousandSeparator=' ' value={offer.get("price")} suffix="zł"
                                  displayType={'text'}/>
                </p>
              </div>
            </div>
            <div className="form-group">
              <label className="col-sm-2 control-label">Zdjęcie:</label>
              <div className="col-sm-10">
                <p className="form-control-static">
                  <img src={offer.get("imageUrl")}/>
                </p>
              </div>
            </div>
          </form>
        </Col>
      </Row>
    )
  }
}

OfferDetails.propTypes = {
  offer: PropTypes.object.isRequired
};

export default OfferDetails;