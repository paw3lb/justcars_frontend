import React from 'react';
import PropTypes from 'prop-types';

export class OfferGallery extends React.Component {

  render() {
    const { offers } = this.props;

    if(!offers) return null;

    return (
      <div className="offer-gallery well"></div>
    )
  }

}

OfferGallery.propTypes = {
  offers: PropTypes.object
};

export default OfferGallery;