export default function externalApi() {
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;

  return {
    url: apiUrl
  }
}