import { schema } from 'normalizr';

export function offerSchema() {
  return new schema.Entity('offer');
}

export function offersSchema() {
  const offer = new schema.Entity('offers');
  return { offers: [ offer ] };
}

