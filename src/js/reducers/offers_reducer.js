import { FETCH_OFFER, FETCH_OFFERS } from '../actions/action_types';
import { normalize } from 'normalizr';
import { offersSchema, offerSchema } from '../config/schema';

const { fromJS } = require('immutable');

export default function(state = {}, action) {
  switch (action.type) {
    case `${FETCH_OFFERS}_FULFILLED`: {
      const normalizedData = normalize(action.payload.data, offersSchema());
      const data = fromJS(normalizedData);
      return {
        ...state,
        all: data.get('entities').get("offers")
      }
    }
    case `${FETCH_OFFER}_FULFILLED`: {
      const normalizedData = normalize(action.payload.data.offer, offerSchema());
      const data = fromJS(normalizedData);
      const entities = data.get('entities');
      const offer = entities.get("offer").first();

      return {
        ...state,
        offer: offer
      }
    }
  }

  return state;
}