import { combineReducers } from "redux";
import offers from "./offers_reducer";

export default combineReducers({ offers })