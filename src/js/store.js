import { applyMiddleware, createStore, compose } from "redux";
import promise from "redux-promise-middleware";
import thunk from "redux-thunk";
import reducer from "./reducers";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //Google chrome dev tool (from google store)
const middleware = applyMiddleware(thunk, promise());

export default createStore(reducer, composeEnhancers(middleware));