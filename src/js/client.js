import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from "react-redux";
import store from "./store";
import Layout from "./layouts/layout";
import OfferListPage from "./components/pages/offer_list_page";
import OfferViewPage from "./components/pages/offer_view_page";

require('../css/styles.sass');

const app = document.getElementById('app');
const offerPath = '/offers/:id';
const offersPath = '/';

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route path={offerPath} component={OfferViewPage} />
          <Route exact path={offersPath} component={OfferListPage} />
        </Switch>
      </Layout>
    </BrowserRouter>
  </Provider>
  , app);