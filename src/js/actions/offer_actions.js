import axios from "axios";
import externalApi from "../config/external_api";
import { FETCH_OFFER, FETCH_OFFERS } from './action_types';
import QueryString from 'query-string';

export function fetchOffers(params) {
  return {
    type: FETCH_OFFERS,
    payload: axios.get(`${externalApi().url}/offers?${QueryString.stringify(params)}`)
  }
}

export function fetchOffer(id) {
  return {
    type: FETCH_OFFER,
    payload: axios.get(`${externalApi().url}/offers/${id}`)
  }
}