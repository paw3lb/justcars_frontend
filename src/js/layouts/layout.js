import React from "react";
import { Grid, Row, Col } from 'react-bootstrap';
import Header from "../components/header/header"

export default class Layout extends React.Component {

  render() {
    return (
      <Grid fluid>
        <Row>
          <Col xs={12} md={12} lg={12}>
            <Header/>
            {this.props.children}
          </Col>
        </Row>
      </Grid>
    );
  }

}
