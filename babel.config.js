module.exports = function (api) {
  api.cache(true);

  const presets = [
    '@babel/preset-env',
    "@babel/preset-react"
  ];
  const plugins = [
    "@babel/plugin-transform-modules-commonjs", "@babel/plugin-syntax-dynamic-import", "@babel/plugin-proposal-class-properties",
    "@babel/plugin-proposal-export-namespace-from", "@babel/plugin-proposal-throw-expressions", "@babel/plugin-transform-react-constant-elements",
    "@babel/plugin-transform-react-jsx", "@babel/plugin-proposal-export-default-from"
  ];

  return {
    presets,
    plugins
  };
}