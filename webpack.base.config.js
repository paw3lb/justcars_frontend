const path    = require('path');
const webpack = require('webpack');
const dotenv  = require('dotenv');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = env => {
  const platform = env.ENVIRONMENT;
  const envPath = path.join(__dirname, '.env.' + platform);
  const fileEnv = dotenv.config({path: envPath}).parsed;
  const envKeys = Object.keys(fileEnv).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(fileEnv[next]);
    return prev;
  }, {});

  return {
    entry: './src/js/client.js',
    module: {
      rules: [
        {
          test: /\.js?$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.(scss|sass)$/i,
          use: [
            platform === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
            'css-loader',
            'sass-loader'
          ]
        },
        {
          test: /\.css$/,
          use: [
            platform === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
            'css-loader'
          ]
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf|svg|mp3)$/,
          loader: "file-loader"
        },
        {
          test: /\.(gif|jpg|jpeg|png|ico)$/,
          loader: "file-loader"
        }
      ]
    },
    output: {
      path: path.resolve(__dirname, "./dist"),
      filename: "js/client.min.js",
    },
    plugins: [
      new webpack.DefinePlugin(envKeys)
    ]
  };
};