const merge                   = require('webpack-merge');
const MiniCssExtractPlugin    = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin       = require('html-webpack-plugin');
const UglifyJsPlugin          = require('uglifyjs-webpack-plugin');
const baseConfig              = require('./webpack.base.config');
const path                    = require('path');

const prodConfiguration = env => {
  return merge([
    {
      mode: 'production',
       optimization: {
         minimizer: [new UglifyJsPlugin()],
       },
      plugins: [
        new MiniCssExtractPlugin(),
        new OptimizeCssAssetsPlugin(),
        new HtmlWebpackPlugin({
          template: path.join(__dirname, './dist/index.html'),
          filename: "index.html"
        })
      ],
      output: {
        publicPath: "/"
      }
    },
  ]);
};

module.exports = env => {
  return merge(baseConfig(env), prodConfiguration(env));
};